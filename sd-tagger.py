import requests
import json
import base64
import sys
import os
import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
nltk.download('punkt')
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')
nltk.download('stopwords')



# sd-tagger
# (c) Andreas Niggemann, Speyer 2023
#

# Stable Diffusion must be running
# Generate an image list by executing following command: dir *.jpg /s/b >imglist.txt
# Start sd-tagger with parameter imglist.txt. Like so: python sd-tagger.py imglist.txt >iptc.csv
# Don't do this in PowerShell, use the CMD shell, otherwise iptc.csv cannot be processed by EXIFTool
# Resulting list is compatible with EXIFTool CSV import. Example:
# exiftool -csv=iptc.csv C:\imagedir -r -overwrite_original
# The resulting prompt can now be found in the IPTC Caption

url = "http://127.0.0.1:7860/sdapi/v1/interrogate"
imagelist = []


def process_image(img_filename):
	retval = ''
	if os.path.isfile(img_filename):
		with open(img_filename, 'rb') as f:
			encoded_string = base64.b64encode(f.read()).decode('utf-8')
		payload = {"image": encoded_string, "model": "clip"}
		response = requests.post(url, headers={'Content-Type': 'application/json'}, data=json.dumps(payload))
		if response.status_code == 200:
			retval = response.json()["caption"].split(",")[0]
		else:
			print("Error calling API for " + img_filename + " : ", response.text, file=sys.stderr)
	return retval


def traverse_tree(tree):
	retval = ''
	for subtree in tree:
		if type(subtree) == nltk.tree.Tree and subtree.label() == 'NP':
			retval += ''.join(''.join(elems) for elems in subtree)
	return retval

def get_keywords(sentence):
	retval = ''
	words = word_tokenize(sentence)
	words_clean = set(stopwords.words("english"))
	filtered_list = [word for word in words if word.casefold() not in words_clean]
	lemmatizer = WordNetLemmatizer()
	lem_words = [lemmatizer.lemmatize(word) for word in filtered_list]
	words_clean = nltk.pos_tag(lem_words)
	grammar = "NP: {<DT>?<JJ>*<NN>}"
	chunk_parser = nltk.RegexpParser(grammar)
	tree = chunk_parser.parse(words_clean)
	retval = traverse_tree(tree)
	retval = retval.replace('NN',',')
	retval = retval.replace('JJ',' ')
	return retval[:-1]


imagelist_filename = sys.argv[1].strip()
if imagelist_filename != '':
	if os.path.isfile(imagelist_filename):
		with open(imagelist_filename, 'r') as ilist_file:
			imagelist = [line.strip() for line in ilist_file.readlines()]

if len(imagelist) > 0:
	print('"SourceFile","Caption-Abstract","Keywords"')
	for image_filename in imagelist:
		if image_filename.split(".")[-1].lower() in ['jpg', 'jpeg', 'jpe']:
			prompt = process_image(image_filename)
			if prompt != '':
				keywords = get_keywords(prompt)
				print('"' + image_filename + '","' + prompt + '","' + keywords + '"')
