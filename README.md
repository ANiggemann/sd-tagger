# SD-Tagger
SD-Tagger is a CLI module to utilize [Stable Diffusion](https://github.com/AUTOMATIC1111/stable-diffusion-webui) to detect the content of images and generate the prompt to replicate similar images.

SD-Tagger Features
-
* Python module 
* Runs as CLI program 
* SD processing from a list of image files
* Utilizes the CLIP part of SD to detect the image content
* Batch processing of images
* Output is CSV suitable for EXIFTool
* EXIFTool can write the content description to the IPTC field Caption-Abstract of the image file

Installation
-
* Install CUDA suitable for your Graphics Card
* Install Python and Stable Diffusion according to [Stable Diffusion webui](https://github.com/AUTOMATIC1111/stable-diffusion-webui)
* Run SD with the command line parameter --api
* Copy the file sdf-tagger.py to an arbitrary directory
* Python must be in your path and associated with the file extension py
* Write or generate a List of image files

Usage
-
Parameters:<br>
&nbsp;&nbsp;&nbsp;&nbsp;python sd-tagger.py imagelist-filename<br>
&nbsp;&nbsp;&nbsp;&nbsp;Examples:<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;python sd-tagger.py imglist.txt<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;python sd-tagger.py imglist.txt >iptc.csv<br>
<br>
Generate an image list by executing following command: dir *.jpg /s/b >imglist.txt<br>
Don't do this in PowerShell, use the cmd shell, otherwise iptc.csv cannot be processed by EXIFTool<br>
Resulting list is compatible with EXIFTool CSV import. Example:<br>
exiftool -csv=iptc.csv C:\imagedir -r -overwrite_original<br>
The resulting prompt can now be found in the IPTC Caption of the images<br>
